/**
 * @file parrondo.cpp
 * @brief Simulation of Parrondo's paradox.
 * @date Apr 6, 2013
 * @author Jonathan Lenz
 */

/*
 *------------------------------------------------------------------------------
 *   Include Files
 *------------------------------------------------------------------------------
 */

#include <cstddef>
#include <iostream>
#include <cstdio>
#include <getopt.h>

#include <cstdlib>
#include <random>
#include <functional>

using std::endl;
using std::cout;
using std::cerr;

/*
 *------------------------------------------------------------------------------
 *   Private Defines
 *------------------------------------------------------------------------------
 */

/*
 *------------------------------------------------------------------------------
 *   Private Macros
 *------------------------------------------------------------------------------
 */

/*
 *------------------------------------------------------------------------------
 *   Private Data Types
 *------------------------------------------------------------------------------
 */

class SimulationOptions
{
public:
   SimulationOptions() : trials_(100), completion_(false), killProgram_(false) {}
   virtual ~SimulationOptions() {}
   
   void NumTrialsIs(unsigned trials) { trials_ = trials; }
   unsigned NumTrials() const { return trials_; }
   
   void RunToCompletionIs(bool completion) { completion_ = completion; }
   bool RunToCompletion() const { return completion_; }
   
   void EndProgramExecutionIs(bool end) { killProgram_ = end; }
   bool EndProgramExecution() const { return killProgram_; }
   
private:
   unsigned trials_;
   bool completion_;
   bool killProgram_;
};

class SimulationEngine
{
public:
   SimulationEngine() { ResetSimulation(); }
   virtual ~SimulationEngine() {}
   
   /**
    * NextResultIs()
    * @brief Informs the simulation of a new simulation result.
    * @param result the latest simulation result.
    * @post Updates internal state so variance and mean incorporate the new result. 
    */
   void NextResultIs(double result)
   {
      if (numTrials_ == 0)
      {
         sum_ = result;
      }
      else
      {
         /* Update the sample variance */
         double tmp = sum_ - (numTrials_ * result);
         numTrialsTimesVariance_ += (tmp / numTrials_) * (tmp / (numTrials_ + 1));
         
         /* Update the sample running sum. */
         sum_ += result;
      }
      
      numTrials_++;
   }
   
   /**
    * SampleVariance()
    * @brief Returns the current mean (average) of all trials.
    */ 
   double SampleMean() const { return sum_/numTrials_; }
   
   /**
    * SampleVariance()
    * @brief Returns the current variance of all trials.
    */  
   double SampleVariance() const
   {
      return (numTrials_ == 0) || (numTrials_ == 1) ? 
         0 : numTrialsTimesVariance_ / (numTrials_ - 1);
   }
   
   /** 
    * NumConfidenceTrials()
    * @brief Generate number of trials based on the current state of the 
    *   simulation for 95% confidence that we are within 5% of the true 
    *   answer. 
    * @return Number of required trials.
    * @warning A non-zero variance is necessary to return a non-zero number of samples.
    */
   unsigned NumConfidenceTrials(void)
   {
      const double Z95 = 1.960;
      const double epsilon = 0.05;
      double avg = SampleMean();
      
      if (SampleVariance() == 0)
      {
         cerr << "Warning: SimulationEngine:NumConfidenceTrials() calculated number of \n"
            "expected trials with 0 variance. Consider running more trials.\n";
         return 0;
      }
      
      /* The + 1 is an implicit "round up" so I don't have to include the 
       * cmath library. Will always be positive since variance is always 
       * positive and the other values are squared. */
      return (SampleVariance() * Z95 * Z95)
         / (epsilon * epsilon * avg * avg) + 1;
   }
   
   /**
    * ResetSimulation()
    * @brief Resets the state of the simulation.
    * @post All private state variables are reset to where they were at construction.
    */
   void ResetSimulation(void)
   {
      sum_ = 0; numTrialsTimesVariance_ = 0; numTrials_ = 0;
   }
   
private:
   double sum_;
   double numTrialsTimesVariance_;
   unsigned numTrials_;
};

class LinearCongrugentialGenerator
{
public:
   LinearCongrugentialGenerator() {}
   virtual ~LinearCongrugentialGenerator() {}
   
   /**
    * @brief Returns the next random variable of the generator.
    * @return The next random number.
    */
   uint_fast32_t operator()()
   {
      return 0;
   }
   
   /**
    * @brief Seeds the generator with the provided value.
    * @param x the seed value.
    */
   void seed(uint_fast32_t x)
   {
      
   }
   
private:
   
};

template <typename Generator>
class BernoulliDistribution
{
public:
   BernoulliDistribution(double p = 0.5) { }
   virtual ~BernoulliDistribution() {}
   
   bool operator()(Generator prng)
   {
      return true;
   }
   
private:
   
};

class ParrondosGame
{
public:
   ParrondosGame()
   {
      for (signed i = 0; i < NumCoins; i++)
      {
         /* Seed RNGs at orthonginal points in the RNG cycle. */
         rng_[i].seed(0x40000000 * i);
      }
      
      coins_[CoinIndex495] = std::bind(std::bernoulli_distribution(0.495), rng_[CoinIndex495]);
      coins_[CoinIndex095] = std::bind(std::bernoulli_distribution(0.095), rng_[CoinIndex095]);
      coins_[CoinIndex745] = std::bind(std::bernoulli_distribution(0.745), rng_[CoinIndex745]);
      coins_[CoinIndex500] = std::bind(std::bernoulli_distribution(0.500), rng_[CoinIndex500]);
   }
   virtual ~ParrondosGame() {}
   
   signed NextResult()
   {
      signed capital = 0;
      
      /* Implementation here. */
      
      return capital;
   }
   
private:
   enum
   {
      CoinIndex495 = 0,
      CoinIndex095,
      CoinIndex745,
      CoinIndex500,
      NumCoins
   };
   
   std::mt19937 rng_[NumCoins];
   std::function<bool()> coins_[NumCoins];
};

/*
 *------------------------------------------------------------------------------
 *   Public Variables
 *------------------------------------------------------------------------------
 */

/*
 *------------------------------------------------------------------------------
 *   Private Variables
 *------------------------------------------------------------------------------
 */

/*
 *------------------------------------------------------------------------------
 *   Public Constants
 *------------------------------------------------------------------------------
 */

/*
 *------------------------------------------------------------------------------
 *   Private Constants
 *------------------------------------------------------------------------------
 */

/*
 *------------------------------------------------------------------------------
 *   Private Function Prototypes
 *------------------------------------------------------------------------------
 */

static void parseProgramOptions(int argc, char* argv[], SimulationOptions* opt); 

/*
 *------------------------------------------------------------------------------
 *   Public Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * main()
 * @brief This function is the main entry point of the Parrondo's paradox simulation. 
 * @param argc number of command line arguments program was called with.
 * @param argv array of C strings of the command line parameters. 
 * @return EXIT_SUCCESS on successful run.
 */
int main(int argc, char* argv[])
{
   SimulationOptions simOptions;
   SimulationEngine simEngine;
   ParrondosGame theGame;
   
   parseProgramOptions(argc, argv, &simOptions);
   
   if (simOptions.EndProgramExecution()) { return EXIT_SUCCESS; }
   
   for (unsigned j = 0; j < simOptions.NumTrials(); j++)
   {
      simEngine.NextResultIs(theGame.NextResult());
   }
   
   if (!simOptions.RunToCompletion())
   {
      cout << "Game average winnings: " << simEngine.SampleMean() << endl;
      cout << "Sample variance: " << simEngine.SampleVariance() << endl;
   }
   else
   {
      unsigned numTrials = simEngine.NumConfidenceTrials();
      simEngine.ResetSimulation();
      
      cout << "Performing " << numTrials << " runs to get convergence." << endl;
      
      for (unsigned i = 0; i < numTrials; i++)
      {
         simEngine.NextResultIs(theGame.NextResult());
      }
      
      cout << "Game average winnings within 5% with 95% confidence is : "
         << simEngine.SampleMean() << endl;
   }
   
   return EXIT_SUCCESS;
}

/*
 *------------------------------------------------------------------------------
 *   Private Function Definitions
 *------------------------------------------------------------------------------
 */

/**
 * parseProgramOptions()
 * @brief Parses the command line arguments of the program and updates the simulation options accordingly.
 * @param argc Number of command line arguments sent to program.
 * @param argv C-String array of command line arguments.
 * @param opt Simulation options object to be updated.
 * @post Information in opt is updated. 
 */
static void parseProgramOptions(int argc, char* argv[], SimulationOptions* opt)
{
   int c;
   
   while ((c = getopt(argc, argv, "hn:c")) != EOF)
   {
      switch (c)
      {
         case 'h':
            cout << "Usage: parrondo [options]\n";
            cout << "Options: \n";
            cout << "  -h   Print this help message.\n";
            cout << "  -n # Set the number of trials to run (100 by default).\n";
            cout << "  -c   Run the program to 5% convergence with 95% confidence\n";
            cout << "       using the number of trials specified by -n.\n";
            opt->EndProgramExecutionIs(true);
            break;
            
         case 'n':
            opt->NumTrialsIs(atoi(optarg));
            break;
            
         case 'c':
            opt->RunToCompletionIs(true);
            break;
            
         case '?':
         default:
            cerr << "For program help use -h." << endl;
            opt->EndProgramExecutionIs(true);
            break;
      }
   }
}
