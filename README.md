# Parrondo's Paradox Simulation

This program was written for the Simulation and Modeling Part 1 course of the
Software Developer's Symposium as an example simulation project. See 
[this Wikipedia page](http://en.wikipedia.org/wiki/Parrondo%27s_paradox)
for more background about the problem.

## Program Help String

Program requires GCC version 4.3 or newer and make in order to compile. Older 
versions of GCC can checkout the boost branch use the boost libraries to 
compile instead.

    Usage: parrondo [options]
    Options:
      -h   Print this help message.
      -n # Set the number of trials to run (100 by default).
      -c   Run the program to 5% convergence with 95% confidence
           using the number of trials specified by -n.

## Exercise One

Discover the estimated winnings after 100 iterations through the game. 
Psuedo-code as follows:

* If a fair coin comes up heads
    * Flip a weighted coin with heads probability 0.495. If heads, add 1 to 
      winnings. If tails, subtract 1.
* Else
    * If winnings is divisible by three, flip a coin with heads probability 
      0.095. If heads, add 1 to winnings. If tails, subtract 1.
    * Else, flip a coin with heads probability of 0.745.  If heads, add 1 to 
      winnings. If tails, subtract 1 from winnings.

This functionality should be achievable by only modifying the `NextResult` 
function in the `ParrondosGame` object.  Coins have been setup already in code 
and can be utilized in the following manner:

    coins_[CoinIndex495]() // Returns true if heads, false otherwise.

## Exercise Two

Remove &lt;random&gt; or &lt;boost/random.hpp&gt; include and implement the 
random number generator and Bernoulli distribution functions. Implement a 
linear congruential generator of the form:
    
    x_n+1 = (a*x_n + c) % m

With a = 16,807, c = 0, and m = 2,147,483,647 (0x7FFFFFFF). Remember to set 
your seed to something non-zero or the pseudo-random number generator will get 
stuck and only return 0. 

Feel free to modify the `ParrondosGame`, `BinomialDistribution`, and 
`LinearCongrugentialGenerator` objects as you see fit to make a design that 
makes sense to you.
