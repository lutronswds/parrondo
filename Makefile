UNAME_OUTPUT = "$(shell uname -s)"
MINGW_STR = MINGW
CYGWIN_STR = CYGWIN
LINUX_STR = Linux

ifeq ($(findstring $(MINGW_STR),$(UNAME_OUTPUT)),$(MINGW_STR))
	# Special MinGW options here
	
	# Workaround for MinGW since dynamic linking of std library is broken X_X
	LDFLAGS += -static
endif
ifeq ($(findstring $(CYGWIN_STR),$(UNAME_OUTPUT)),$(CYGWIN_STR))
	# Special Cygwin options here
endif
ifeq ($(findstring $(LINUX_STR),$(UNAME_OUTPUT)),$(LINUX_STR))
	# Special Linux options here
endif

CXXFLAGS += -std=c++0x -Wall

all: parrondo

parrondo: parrondo.o
	$(LINK.o) -o $@ $^ -lstdc++

%.o: %.cpp
	$(COMPILE.cpp) $(OUTPUT_OPTION) $<

clean:
	rm -f *.o parrondo.exe parrondo
